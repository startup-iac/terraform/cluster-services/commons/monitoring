# For more info view:
# https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack

locals {
  secret_name = "${var.dns_name}-tls"
  values_file = "values-${var.chart_version}.yaml"
}

data "template_file" "values" {
  template = "${file("${path.module}/${local.values_file}")}"
  vars = {
    DNS_NAME                    = var.dns_name
    SECRET_NAME                 = local.secret_name
    CLUSTER_ISSUER              = var.cluster_issuer_name
    SUBJECT_ORGANIZATIONS       = var.subject_organizations
    SUBJECT_ORGANIZATIONALUNITS = var.subject_organizationalunits
  }
}

resource "helm_release" "this" {
  name             = "monitoring"
  namespace        = var.namespace
  version          = var.chart_version

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"

  values = [
    data.template_file.values.rendered
  ]

}


